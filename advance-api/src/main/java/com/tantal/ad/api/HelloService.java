package com.tantal.ad.api;

public interface HelloService {

    String sayHello(String name);
}
