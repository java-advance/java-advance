package com.tantal.ad.rpc;

import com.tantal.ad.api.HelloService;
import com.tantal.ad.api.impl.HelloServiceImpl;

/**
 * 服務暴露
 */
public class RpcProvider {

    public static void main(String[] args) throws Exception {
        HelloService service = new HelloServiceImpl();
        RpcFramework.export(service, 1234);
    }
}
