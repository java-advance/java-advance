package com.tantal.ad.juc;

import java.util.concurrent.atomic.AtomicInteger;

public class ThreadLocalDemo {

    static class ThreadLocalInner {
        private final int threadLocalHashCode = nextHashCode();

        private static final int HASH_INCREMENT = 0x61c88647;

        private static AtomicInteger nextHashCode = new AtomicInteger();

        private static int nextHashCode() {
            return nextHashCode.getAndAdd(HASH_INCREMENT);
        }
    }

    public static void main(String[] args) {
        ThreadLocalInner atomicDemo = new ThreadLocalInner();
        System.out.println(atomicDemo.threadLocalHashCode);
        System.out.println(atomicDemo.threadLocalHashCode);
        System.out.println("-----------------------");
        ThreadLocalInner atomicDemo1 = new ThreadLocalInner();
        System.out.println(atomicDemo1.threadLocalHashCode);
        System.out.println(atomicDemo1.threadLocalHashCode);
        System.out.println("-----------------------");
        ThreadLocalInner atomicDemo2 = new ThreadLocalInner();
        System.out.println(atomicDemo2.threadLocalHashCode);
        System.out.println("-----------------------");
        ThreadLocalInner atomicDemo3 = new ThreadLocalInner();
        System.out.println(atomicDemo3.threadLocalHashCode);
        System.out.println("-----------------------");
        ThreadLocalInner atomicDemo4 = new ThreadLocalInner();
        System.out.println(atomicDemo4.threadLocalHashCode);

        /*
        结果如下: （保证了每个线程的hashCode获取的都不一样，同一个线程的hashCode是一致的）
            0
            0
            -----------------------
            1640531527
            1640531527
            -----------------------
            -1013904242
            -----------------------
            626627285
        */
    }
}
