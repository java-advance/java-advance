package com.tantal.ad.jvm;

/**
 * 验证ClassLoader的体系结构即：
 * 类加载器之间的父子关系(注意不是类的集继承关系)
 */
public class ClassLoaderStructure {

    public static void main(String[] args) {
        ClassLoader classLoader = ClassLoaderStructure.class.getClassLoader();

        while (classLoader != null) {
            System.out.println(classLoader.getClass().getName());
            classLoader = classLoader.getParent();
        }

        System.out.println(classLoader);
    }

    /*
    运行结果分析：
    sun.misc.Launcher$AppClassLoader
    sun.misc.Launcher$ExtClassLoader
    null

    1.第一行结果表示ClassLoaderTest类加载器是AppClassLoader
    2.第二行的结果表示AppClassLoader的类加载器是ExtClassLoader
    3.第三行的结果表示ExtClassLoader的类加载器是BootstrapClassLoader

    可以尝试将ClassLoaderStructure.jar打包放到JRE\lib\ext\目录下
    运行结果会是：
        sun.misc.Launcher$ExtClassLoader
        null

     */
}
