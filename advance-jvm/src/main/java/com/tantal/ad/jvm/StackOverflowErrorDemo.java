package com.tantal.ad.jvm;

/**
 * 初始化时机问题，导致堆栈溢出
 *
 * 答案：
 *  即便你在构造函数外面，显式的初始化了一个成员如instance,但是类编译后运行时，
 *  这种显式初始化成员的真正初始化还是放在构造函数中，统一进行的。
 */
public class StackOverflowErrorDemo {

    private StackOverflowErrorDemo instance=new StackOverflowErrorDemo();

    public static void main(String[] args) {
        StackOverflowErrorDemo test = new StackOverflowErrorDemo();

    }
}
