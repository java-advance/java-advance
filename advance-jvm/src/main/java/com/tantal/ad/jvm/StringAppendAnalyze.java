package com.tantal.ad.jvm;

/**
 * 字符串拼接字节码分析
 */
public class StringAppendAnalyze {

    public String testString(String str1, String str2) {
        return str1 + str2;
    }

    public String testStringBuilder(StringBuilder sb, String str) {
        return sb.append(str).toString();
    }

    public String testStringBuffer(StringBuffer sb, String str) {
        return sb.append(str).toString();
    }

    /*执行：
        javap -c StringAppendAnalyze.class >> abc.txt

        最后发现testString是创建了StringBuilder然后执行的append，故直接用StringBuilder效率好
     */

}
