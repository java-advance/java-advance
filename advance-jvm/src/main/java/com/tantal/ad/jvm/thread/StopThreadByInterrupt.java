package com.tantal.ad.jvm.thread;

import java.util.concurrent.TimeUnit;

public class StopThreadByInterrupt {

    public static void main(String[] args) throws InterruptedException {
        Thread thread=new Thread(()->{
            while (true){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    //抛出此异常时，会将interrupt设置为false
                    e.printStackTrace();
                }
            }
        });

        thread.start();
        TimeUnit.SECONDS.sleep(1);

        thread.interrupt();//设置复位标识为true
        TimeUnit.SECONDS.sleep(1);

        System.out.println(thread.isInterrupted());//false

    }
}
