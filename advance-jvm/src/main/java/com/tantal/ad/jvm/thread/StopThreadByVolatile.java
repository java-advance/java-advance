package com.tantal.ad.jvm.thread;

public class StopThreadByVolatile {

    private volatile static boolean stop = false;

    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(() -> {

            long i = 0;
            while (!stop) {
                i++;
            }
            System.out.println(i);
        });

        thread.start();
        System.out.println("begin start thread");
        Thread.sleep(1000);
        stop = true;
    }
}
