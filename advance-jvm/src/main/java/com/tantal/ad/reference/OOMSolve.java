package com.tantal.ad.reference;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;

public class OOMSolve {

    //使用软引用缓存图片文件，在内存不足时会自动回收，规避OOM异常
    private Map<String, SoftReference<byte[]>> imageCache = new HashMap<>();

    public void addBitmapToCache(String path) {

        // 强引用的Bitmap对象
        byte[] imageStream = null;

        // 软引用的Bitmap对象
        SoftReference<byte[]> softBitmap = new SoftReference<>(imageStream);

        // 添加该对象到Map中使其缓存
        imageCache.put(path, softBitmap);

    }

    public byte[] getBitmapByPath(String path) {

        // 从缓存中取软引用的Bitmap对象
        SoftReference<byte[]> softBitmap = imageCache.get(path);

        // 判断是否存在软引用
        if (softBitmap == null) {
            return null;
        }

        // 取出Bitmap对象，如果由于内存不足Bitmap被回收，将取得空
        return softBitmap.get();
    }
}
