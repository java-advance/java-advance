package com.tantal.ad.reference;

import java.lang.ref.WeakReference;

/**
 * 弱引用
 */
public class WeakReferenceDemo {

    public static void main(String[] args) {
          /*----------------------------- 字符串常量 ----------------------------------*/
        WeakReference<String> refString = new WeakReference<>("1234");
        System.out.println(refString.get());// 1234

        //通知JVM的gc进行垃圾回收
        System.gc();

        System.out.println(refString.get());//TODO 这里还是1234，为什么？

        /*-----------------------------字符串对象----------------------------------*/

        WeakReference<String> refStringObject = new WeakReference<>(new String("1234"));
        System.out.println(refStringObject.get());//1234

        //通知JVM的gc进行垃圾回收
        System.gc();

        System.out.println(refStringObject.get());//这里是null

           /*-----------------------------Integer常量----------------------------------*/

        WeakReference<Integer> refInteger = new WeakReference<>(1234);
        System.out.println(refInteger.get());//1234

        //通知JVM的gc进行垃圾回收
        System.gc();

        System.out.println(refInteger.get());//这里是null

    }
}
