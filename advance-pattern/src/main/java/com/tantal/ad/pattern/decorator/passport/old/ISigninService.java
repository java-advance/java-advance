package com.tantal.ad.pattern.decorator.passport.old;

/**
 * Created by Tom on 2018/3/17.
 */
public interface ISigninService {
    ResultMsg regist(String username, String password);


    /**
     * 登录的方法
     *
     * @param username
     * @param password
     * @return
     */
    ResultMsg login(String username, String password);
}
