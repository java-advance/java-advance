package com.tantal.ad.pattern.decorator.passport.upgrede;

import com.tantal.ad.pattern.decorator.passport.old.ISigninService;
import com.tantal.ad.pattern.decorator.passport.old.ResultMsg;

/**
 * Created by Tom on 2018/3/17.
 */
public interface ISigninForThirdService extends ISigninService {


    ResultMsg loginForQQ(String openId);

    ResultMsg loginForWechat(String openId);

    ResultMsg loginForToken(String token);

    ResultMsg loginForTelphone(String telphone, String code);
}
