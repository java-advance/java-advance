package com.tantal.ad.pattern.decorator.passport.upgrede;


import com.tantal.ad.pattern.decorator.passport.old.ISigninService;
import com.tantal.ad.pattern.decorator.passport.old.ResultMsg;

/**
 * Created by Tom on 2018/3/17.
 */
public class SigninForThirdService implements ISigninForThirdService {

    private ISigninService service;
    public SigninForThirdService(ISigninService service){
        this.service = service;
    }

    /*--------------------------保留老的方法-----------------------------------------*/
    @Override
    public ResultMsg regist(String username, String password) {
        return service.regist(username,password);
    }

    @Override
    public ResultMsg login(String username, String password) {
        return service.login(username,password);
    }

    /*--------------------------增加新的方法-----------------------------------------*/
    @Override
    public ResultMsg loginForQQ(String openId){

        //4、调用原来的登录方法
        this.regist(openId,null);
        return this.login(openId,null);
    }

    @Override
    public ResultMsg loginForWechat(String openId){
        return null;
    }

    @Override
    public ResultMsg loginForToken(String token){
        //通过token拿到用户信息，然后再重新登陆了一次
        return  null;
    }
    @Override
    public ResultMsg loginForTelphone(String telphone,String code){

        return null;
    }
}
