package com.tantal.ad.pattern.delegate;

public class Boss {

    //客户请求（Boss），委派者（Leader），被委派者（Target）
    //委派者需要持有被委派者的引用
    //代理模式注重过程，委派注重结果
    //策略模式注重可拓展，委派注重内部的灵活和复用

    //委派的核心：静态代理 + 策略模式的一种特殊组合
    public static void main(String[] args) {

        ITarget leader = new Leader();
        leader.doing("登录");
    }
}
