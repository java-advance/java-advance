package com.tantal.ad.pattern.delegate;

public interface ITarget {

    void doing(String command);
}
