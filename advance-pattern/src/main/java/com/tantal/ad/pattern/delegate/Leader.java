package com.tantal.ad.pattern.delegate;

import java.util.HashMap;
import java.util.Map;

/**
 * 项目经理委托类
 */
public class Leader implements ITarget {

    private Map<String,ITarget> targetMapping =new HashMap<>();

    public Leader() {
        targetMapping.put("加密",new TargetA());
        targetMapping.put("登录",new TargetB());
    }

    @Override
    public void doing(String command) {
        //经理自己不干活，委托给别人
        targetMapping.get(command).doing(command);

    }
}
