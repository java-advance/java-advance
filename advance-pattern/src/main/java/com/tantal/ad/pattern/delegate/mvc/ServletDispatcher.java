package com.tantal.ad.pattern.delegate.mvc;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ServletDispatcher extends HttpServlet {

    private static List<Handler> handleMapping = new ArrayList<>();

    public ServletDispatcher() {
        Class tClass = LoginAction.class;
        try {
            handleMapping.add(new Handler()
                    .setController(tClass.newInstance())
                    .setMethod(tClass.getMethod("login", String.class))
                    .setUrl("web/login.json"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doDispatch(req, resp);
    }

    private void doDispatch(HttpServletRequest req, HttpServletResponse resp) {
        String uri = req.getRequestURI();

        //servlet拿到url后需要寻找url对应映射的java方法
        //即：通过url从handleMapping（策略常量）寻找
        Handler handle = handleMapping.stream()
                .filter(it -> uri.equals(it.getUrl()))
                .findFirst().orElseThrow(() -> new RuntimeException("找不到URL映射"));

        try {
            //将具体的任务分发给Method（通过反射去调用其对应的方法)
            Object result = handle.getMethod().invoke(handle.getController(), req.getParameter("name"));

            //获取到Method执行的结果，通过Response返回出去
            resp.getWriter().write(result.toString());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    static class Handler {
        private Object controller;
        private Method method;
        private String url;

        public Object getController() {
            return controller;
        }

        public Handler setController(Object controller) {
            this.controller = controller;
            return this;
        }

        public Method getMethod() {
            return method;
        }

        public Handler setMethod(Method method) {
            this.method = method;
            return this;
        }

        public String getUrl() {
            return url;
        }

        public Handler setUrl(String url) {
            this.url = url;
            return this;
        }
    }
}
