package com.tantal.ad.pattern.observer.simple;

public class App {

    public static void main(String[] args) {
        WechatServer server = new WechatServer();

        Observer zhangsan = new User("张三");
        Observer lisi = new User("李四");
        Observer wangwu = new User("王五");

        //增加观察者(订阅者)
        server.attach(zhangsan);
        server.attach(lisi);
        server.attach(wangwu);

        //公众号开始推送消息
        server.pushNewMessage("PHP是世界上最好用的语言!");

        System.out.println("----------------------------------------------");
        //李四不喜欢PHP，果断退订
        server.detach(lisi);

        //公众号再次推送消息
        server.pushNewMessage("Java是世界上最好用的语言!");
    }
}
