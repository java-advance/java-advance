package com.tantal.ad.pattern.observer.simple;

/**
 * 抽象观察者
 * 定义update()方法，当被观察者调用notify()时，观察者的update()会被回调
 */
public interface Observer {
    void update(String message);
}
