package com.tantal.ad.pattern.observer.simple;

/**
 * 抽象被观察者接口
 * 声明了添加、删除、通知观察者方法
 */
public interface Observerable {
    /**
     * 添加观察者
     *
     * @param observer
     */
    void attach(Observer observer);

    /**
     * 删除观察者
     *
     * @param observer
     */
    void detach(Observer observer);

    /**
     * 通知所有观察者
     *
     * @param message
     */
    void notifyObservers(String message);
}
