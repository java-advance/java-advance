package com.tantal.ad.pattern.observer.simple;

/**
 * 观察者，定义微信公众号的用户
 */
public class User implements Observer {

    private String name;

    public User(String name) {
        this.name = name;
    }

    @Override
    public void update(String message) {

        read(message);

    }

    private void read(String message) {

        System.out.println(name + " 收到推送消息： " + message);
    }
}
