package com.tantal.ad.pattern.observer.simple;

import java.util.ArrayList;
import java.util.List;

/**
 * 被观察者，微信公众订阅号
 * 实现了Observerable接口
 */
public class WechatServer implements Observerable {

    //观察者集合封装
    private List<Observer> observers;

    public WechatServer() {
        this.observers = new ArrayList<>();
    }

    @Override
    public void attach(Observer o) {
        observers.add(o);
    }

    @Override
    public void detach(Observer o) {
        if (observers.isEmpty()) return;
        observers.remove(o);
    }

    @Override
    public void notifyObservers(String message) {

        for (Observer it : observers) {
            it.update(message);
        }
    }

    //推送消息
    public void pushNewMessage(String message) {
        System.out.println("微信服务更新消息： " + message);
        //消息更新，通知所有观察者
        notifyObservers(message);
    }

}
