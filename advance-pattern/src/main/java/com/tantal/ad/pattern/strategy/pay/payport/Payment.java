package com.tantal.ad.pattern.strategy.pay.payport;


import com.tantal.ad.pattern.strategy.pay.PayState;

/**
 * 支付渠道
 * Created by Tom on 2018/3/11.
 */
public interface Payment {

    PayState pay(String uid, double amount);

}
