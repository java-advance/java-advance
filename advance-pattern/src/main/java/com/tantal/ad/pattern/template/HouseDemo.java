package com.tantal.ad.pattern.template;

public class HouseDemo {

    public static void main(String[] args) {
        AbstractHouse house=new SeaHouse();
        house.construct();
    }

    static abstract class AbstractHouse {
        //打地基
        protected abstract void buildFoundations();

        //抹墙
        protected abstract void buildWall();

        public final void construct() {

            /**定义好了步骤和顺序，子类去实现*/

            //先打地基
            buildFoundations();
            //在抹墙
            buildWall();
        }
    }

    class EcologicalHouse extends AbstractHouse {

        @Override
        protected void buildFoundations() {
            System.out.println("Making foundations with wood");
        }

        @Override
        protected void buildWall() {
            System.out.println("Making wall with wood");
        }

    }

    static class SeaHouse extends AbstractHouse {

        @Override
        protected void buildFoundations() {
            System.out.println("Constructing very strong foundations");
        }

        @Override
        protected void buildWall() {
            System.out.println("Constructing very strong wall");
        }

    }

}
