package com.tantal.ad.redission.atomid;

public class DistributedAtomicID {

    public static void main(String[] args) throws InterruptedException {

        final String key = "test123";
        final long wait=1000;
        try {

            long start = System.currentTimeMillis();

            while (true) {
                if (DistributedRedisLock.acquire(key)) {

                    Long result = RedissonManager.nextID();
                    System.out.println(result);
                    break;
                }

                //自旋超时，退出循环
                if (System.currentTimeMillis() - start > wait) {
                    break;
                }
            }
        } finally {
            DistributedRedisLock.release(key);
        }
    }
}
