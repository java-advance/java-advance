package com.tantal.ad.spring.aop;

public class HelloImpl implements IHello {
    @Override
    public void say(String name) {
        System.out.println("Hello " + name);
    }
}
