package com.tantal.ad.spring.aop;

public interface IHello {

    void say(String name);
}
