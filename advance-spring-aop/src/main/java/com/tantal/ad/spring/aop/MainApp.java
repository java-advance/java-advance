package com.tantal.ad.spring.aop;

import com.tantal.ad.spring.aop.proxy.CglibProxy;
import com.tantal.ad.spring.aop.proxy.JdkDynamicProxy;

public class MainApp {

    public static void main(String[] args) {
        JdkDynamicProxy jdkDynamicProxy=new JdkDynamicProxy(new HelloImpl());
        IHello helloProxy = jdkDynamicProxy.getProxy();
        helloProxy.say("dynastqin");

        CglibProxy cglibProxy = new CglibProxy();
        IHello helloProxy2= cglibProxy.getProxy(HelloImpl.class);
        helloProxy2.say("Tantal");
    }
}
