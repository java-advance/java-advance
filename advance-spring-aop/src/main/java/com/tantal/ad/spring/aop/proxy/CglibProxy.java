package com.tantal.ad.spring.aop.proxy;


import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class CglibProxy implements MethodInterceptor {

    public <T> T getProxy(Class<T> tClass){
        return (T) Enhancer.create(tClass,this);
    }

    @Override
    public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        System.out.println("=====方法调用前");
       Object result = methodProxy.invokeSuper(o,args);
        System.out.println("=====方法调用后");
        return result;
    }
}
