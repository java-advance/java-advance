package com.tantal.ad.spring.aopchain;

import java.util.List;

public class Chain {

    private List<Point> list;
    private Object target;
    private int index = -1;

    public Chain(List<Point> list, Object target) {
        this.list = list;
        this.target = target;
    }

    public Object proceed() {
        Object result;
        if (++index == list.size()) {
            result = target.toString();
            System.out.println("Target method invoke result:" + result);
        } else {
            Point point = list.get(index);
            result = point.proceed(this);
        }

        return result;

    }

    public interface Point {
        Object proceed(Chain chain);
    }
}
