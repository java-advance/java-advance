package com.tantal.ad.spring.aopchain;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class ChainInterceptorInvoke implements MethodInterceptor {

    private Chain chain;

    public ChainInterceptorInvoke(Chain chain) {
        this.chain = chain;
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        return chain.proceed();
    }
}
