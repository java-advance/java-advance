package com.tantal.ad.spring.aopchain;

public class ChainMain {

    public static void main(String[] args) {

        Object proxy = ProxyFactory.create().getProxy(new SayHello());
        proxy.toString();
    }

    static class SayHello{

        public String hello(){
            return "hello cglib!";
        }
    }
}
