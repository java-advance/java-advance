package com.tantal.ad.spring.aopchain;

public class PointInstancesA implements Chain.Point{
    @Override
    public Object proceed(Chain chain) {
        System.out.println("point A before");
        Object result = chain.proceed();
        System.out.println("point A after");
        return result;
    }
}
