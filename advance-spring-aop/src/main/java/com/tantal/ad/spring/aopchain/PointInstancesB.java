package com.tantal.ad.spring.aopchain;

public class PointInstancesB implements Chain.Point{
    @Override
    public Object proceed(Chain chain) {
        System.out.println("point B before");
        Object result = chain.proceed();
        System.out.println("point B after");
        return result;
    }
}
