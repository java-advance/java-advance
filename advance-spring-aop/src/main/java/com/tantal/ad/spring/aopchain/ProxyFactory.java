package com.tantal.ad.spring.aopchain;

import net.sf.cglib.proxy.Enhancer;

import java.util.Arrays;
import java.util.List;

public class ProxyFactory {

    private ProxyFactory() {}

    public static ProxyFactory create() {
        return new ProxyFactory();
    }

    public Object getProxy(Object origin) {
        final Enhancer en = new Enhancer();
        en.setSuperclass(origin.getClass());

        //增加拦截器链
        List<Chain.Point> list = Arrays.asList(new PointInstancesA(), new PointInstancesB());

        en.setCallback(new ChainInterceptorInvoke(new Chain(list, origin)));
        return en.create();
    }
}
