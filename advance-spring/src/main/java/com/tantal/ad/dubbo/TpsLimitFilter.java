package com.tantal.ad.dubbo;

import com.alibaba.dubbo.common.Constants;
import com.alibaba.dubbo.common.extension.Activate;
import com.alibaba.dubbo.rpc.*;
import com.alibaba.dubbo.rpc.filter.tps.DefaultTPSLimiter;
import com.alibaba.dubbo.rpc.filter.tps.TPSLimiter;

//參考：com.alibaba.dubbo.rpc.filter.TpsLimitFilter
@Activate(group = Constants.PROVIDER, value = Constants.TPS_LIMIT_RATE_KEY)
public class TpsLimitFilter implements Filter {

    private final TPSLimiter tpsLimitFilter = new DefaultTPSLimiter();

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {

        //根据 tps 限流规则判断是否限制此次调用。若是，抛出 RpcException 异常
        if (!tpsLimitFilter.isAllowable(invoker.getUrl(), invocation)) {
            throw new RpcException("Failed to invoke service " + invoker.getInterface().getName() +
                    "." + invocation.getMethodName()
                    + " because exceed max service tps.");
        }
        // 服务调用
        return invoker.invoke(invocation);
    }
}
