package com.tantal.ad.spring;

import com.tantal.ad.spring.service.BankService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringBootstrap {

    public static void main(String[] args) {

        ClassPathXmlApplicationContext ctx=new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        ctx.start();
        BankService bean = ctx.getBean(BankService.class);
        System.out.println(bean.getBalanceByEmail("abc"));
    }
}
