package com.tantal.ad.spring.dao;

public interface BankDao {
    String getBalanceByEmail(String email);
}
