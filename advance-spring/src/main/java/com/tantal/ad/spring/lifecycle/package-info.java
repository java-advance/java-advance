package com.tantal.ad.spring.lifecycle;
//展示bean的生命周期 ref ： https://www.cnblogs.com/kenshinobiy/p/4652008.html

/*
    Spring IOC容器基于配置文件（xml或注解）来实例化bean

                        ↓
    DI，set注入

                        ↓
    BeanNameAware # setBeanName() 传递该bean的ID

                        ↓
    BeanFactoryAwre # setBeanFactory() 传递BeanFactory实例对象

                        ↓
    BeanPostProcessor # postProcessBeforeInitialization()

                        ↓
    InitializingBean # afterPropertiesSet()

                        ↓
    调用自定义的init-method

                        ↓
    BeanPostProcessor # postProcessAfterInitialization()

                        ↓
    bean创建完成

=========================================================================
    容器关闭

                        ↓
    @PreDestory注解方法调用

                        ↓
    DisposableBean # destroy()

                        ↓
    调用自定义的destory-method


*/