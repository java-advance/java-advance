package com.tantal.ad.spring.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Data
@Component
public class UserBalance {
    private Long accountNo;
    private String accountName;
    private BigDecimal money;
}
