package com.tantal.ad.spring.service;

public interface BankService {
    String getBalanceByEmail(String email);
    Integer getBalanceById(Integer id);
}