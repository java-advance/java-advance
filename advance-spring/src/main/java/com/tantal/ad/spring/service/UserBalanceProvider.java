package com.tantal.ad.spring.service;

import com.tantal.ad.spring.pojo.UserBalance;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * 每次产生一个新的UserBalance实例，而非spring的单例
 */
@Service("userBalanceProvider")
public class UserBalanceProvider implements ApplicationContextAware {

    private ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        this.context = context;
    }

    public UserBalance getInstance() {
        return context.getBean(UserBalance.class);
    }
}