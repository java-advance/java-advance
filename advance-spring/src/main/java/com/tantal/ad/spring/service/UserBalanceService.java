package com.tantal.ad.spring.service;

public interface UserBalanceService {
    Double getAccountBalance(String email);
}