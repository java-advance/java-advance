package com.tantal.ad.spring.service.impl;

import com.tantal.ad.spring.service.BankService;
import org.springframework.stereotype.Service;

@Service
public class BankServiceImpl implements BankService {
//    @Autowired
//    private BankDao bankDao;

    @Override
    public String getBalanceByEmail(String email) {

        System.out.println("[BankServiceImpl] invoke getBalanceByEmail(" + email + ")");
//        System.out.println(bankDao.getBalanceByEmail(email));
        return "100";
//        throw new UnsupportedOperationException("Operation failed due to external exception");
    }

    @Override
    public Integer getBalanceById(Integer id) {
        System.out.println("[BankServiceImpl] invoke getBalanceById(" + id + ")");
        return id;
    }
}