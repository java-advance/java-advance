package com.tantal.ad.spring.service.impl;

import com.tantal.ad.spring.pojo.UserBalance;
import com.tantal.ad.spring.service.BankService;
import com.tantal.ad.spring.service.UserBalanceProvider;
import com.tantal.ad.spring.service.UserBalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserBalanceServiceImpl implements UserBalanceService {
    @Autowired
    private BankService bankService;

    @Autowired
    private UserBalanceProvider userBalanceProvider;

    @Override
    public Double getAccountBalance(String email) {
        System.out.println("[UserBalanceServiceImpl] invoke getAccountBalance(" + email + ")");
        Integer balanceById = bankService.getBalanceById(100);
        String balanceByEmail = bankService.getBalanceByEmail(email);

        //每次都是一个新的实例，但这和直接new有什么区别呢？
        UserBalance newInstance= userBalanceProvider.getInstance();
        return balanceByEmail==null?null:Double.valueOf(balanceByEmail);
    }
}