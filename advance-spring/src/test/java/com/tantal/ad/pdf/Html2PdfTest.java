package com.tantal.ad.pdf;

import com.itextpdf.text.pdf.BaseFont;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.junit.Test;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by hongzhenyue on 18/2/28.
 */
public class Html2PdfTest {
    @Test
    public void testHtml2Pdf() throws Exception{
        //指定PDF的存放路径
        String outputFile = "H:\\JavaWork\\java-advance\\advance-spring\\src\\test\\resources\\result.pdf";
        OutputStream os = new FileOutputStream(outputFile);
        ITextRenderer renderer = new ITextRenderer();
        ITextFontResolver fontResolver = renderer.getFontResolver();
        //指定字体。为了支持中文字体
        fontResolver.addFont(Html2PdfTest.class.getResource("/").getPath()+"font/ARIALUNI.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

        StringBuffer html = new StringBuffer();

        html.append(renderData());
//        html.append("<!DOCTYPE html>\n" +
//                "<html lang=\"en\">\n" +
//                "<head>\n" +
//                "    <meta charset=\"UTF-8\"></meta>\n" +
//                "    <title>Issue Payment Receipt</title>\n" +
//                "    <style type=\"text/css\">\n" +
//                "    body {\n" +
//                "        font-family: Arial Unicode MS;\n" +
//                "    }" +
//                ".myStyle{ color:red }" +
//                "\n" +
//                "    </style>\n" +
//                "</head>\n" +
//                "<body>\n" +
//                "    <img src=\"file/AIG.png\" style=\"width:160px;height:80px;\"></img>\n" +
//                "    <br/>\n" +
//                "    <br/> 建設銀行\n" +
//                "    <br/> <span class='myStyle'>12345678901</span>\n" +
//                "    <br/> 1000RMB\n" +
//                "    <br/> 姓名:覃涛\n" +
//                "    <br/> 單號:123456\n" +
//                "    <br/>\n" +
//                "</body>\n" +
//                "</html>");
        renderer.setDocumentFromString(html.toString());
        // 解决图片的相对路径问题
        renderer.getSharedContext().setBaseURL("file:/H:/JavaWork/java-advance/advance-spring/src/test/resources/file");
        renderer.layout();
        renderer.createPDF(os);
        renderer.finishPDF();
        renderer = null;
        os.close();
    }

    public static String renderData() {
        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());

//          ve.setProperty(Velocity.FILE_RESOURCE_LOADER_PATH, HelloVelocity.class.getResource("/").getPath() + "webapp/template/");

        ve.setProperty(Velocity.INPUT_ENCODING, "UTF8");
        ve.setProperty(Velocity.OUTPUT_ENCODING,  "UTF8");
        ve.init();

        Template t = ve.getTemplate("file/pdf-template.vm");
        VelocityContext ctx = new VelocityContext();

        ctx.put("bankName", "建設銀行");
        ctx.put("bankNo", 12345678901L);
        ctx.put("date", new Date());

        List temp = new ArrayList();
        temp.add("1");
        temp.add("2");
        ctx.put("list", temp);

        StringWriter sw = new StringWriter();
        t.merge(ctx, sw);

        return sw.toString();
    }
}