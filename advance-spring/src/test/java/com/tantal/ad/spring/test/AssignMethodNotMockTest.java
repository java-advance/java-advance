package com.tantal.ad.spring.test;

import com.tantal.ad.spring.dao.BankDao;
import com.tantal.ad.spring.service.UserBalanceService;
import com.tantal.ad.spring.service.impl.BankServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kubek2k.springockito.annotations.ReplaceWithMock;
import org.kubek2k.springockito.annotations.SpringockitoContextLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * 指定方法不调用mock
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = SpringockitoContextLoader.class, locations = "classpath*:applicationContext.xml")
public class AssignMethodNotMockTest {

    @Autowired
    private UserBalanceService userBalanceService;

    @Autowired
    @ReplaceWithMock
    private BankServiceImpl bankService;

    //因为容器缺失BankDao 这里需要mock注入
    @ReplaceWithMock
    private BankDao bankDao;
    @Before
    public void setUp() throws Exception {

        //指定方法不使用mock
        doCallRealMethod()
                .when(bankService).getBalanceById(anyInt());
        //or
//        given(bankService.getBalanceById(anyInt()))
//                .willCallRealMethod();

    }
    @Test
    public void shouldReturnMockedBalance() {
        Double balance = userBalanceService.getAccountBalance("user@bank.com");
        String balanceByEmail = bankService.getBalanceByEmail("user@bank.com");
        verify(bankService,times(2)).getBalanceByEmail("user@bank.com");

        assertEquals(balance, null);
    }
}