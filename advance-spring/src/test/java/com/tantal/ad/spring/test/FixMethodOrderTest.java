package com.tantal.ad.spring.test;

import com.tantal.ad.spring.dao.BankDao;
import com.tantal.ad.spring.service.UserBalanceService;
import com.tantal.ad.spring.service.impl.BankServiceImpl;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@FixMethodOrder(MethodSorters.JVM)//指定测试方法按定义的顺序执行
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class FixMethodOrderTest {

    @Autowired
    private UserBalanceService userBalanceService;

    @InjectMocks
    @Mock
    private BankServiceImpl bankService;

    @Mock
    private BankDao bankDao;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testA(){
        System.out.println("testA");
//        given(bankService.getBalanceByEmail("user@bank.com")).willReturn(String.valueOf(valueOf(123.45D)));
        when(bankService.getBalanceByEmail(anyString())).thenReturn(String.valueOf(123.45D));
//        doCallRealMethod()
//                .when(bankService).getBalanceById(anyInt());
        Double balance = userBalanceService.getAccountBalance("user@bank.com");
        System.out.println(balance);
    }

    @Test
    public void testC(){
        System.out.println("testC");
    }

    @Test
    public void testB(){
        System.out.println("testB");
    }

}
