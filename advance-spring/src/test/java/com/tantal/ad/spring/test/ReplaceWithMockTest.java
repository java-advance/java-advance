package com.tantal.ad.spring.test;

import com.tantal.ad.spring.dao.BankDao;
import com.tantal.ad.spring.service.BankService;
import com.tantal.ad.spring.service.UserBalanceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kubek2k.springockito.annotations.ReplaceWithMock;
import org.kubek2k.springockito.annotations.SpringockitoContextLoader;
import org.kubek2k.springockito.annotations.experimental.junit.AbstractJUnit4SpringockitoContextTests;
import org.kubek2k.tools.Ordered;
import org.kubek2k.tools.OrderedSpringJUnit4ClassRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import static java.lang.Double.valueOf;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@RunWith(OrderedSpringJUnit4ClassRunner.class)//与DirtiesMocks配合使用，这里已经继承了SpringJUnit4ClassRunner
@ContextConfiguration(loader = /*使注解生效*/SpringockitoContextLoader.class, locations = "classpath*:applicationContext.xml")
//@DirtiesMocks(classMode = DirtiesMocks.ClassMode.AFTER_CLASS)//class级别的还原
//@DirtiesMocks(classMode = DirtiesMocks.ClassMode.AFTER_EACH_TEST_METHOD)//方法级别的还原
public class ReplaceWithMockTest extends AbstractJUnit4SpringockitoContextTests {

    @Autowired
    private UserBalanceService userBalanceService;

    @Autowired
    @ReplaceWithMock
    private BankService bankService;

    //因为容器缺失BankDao 这里需要mock注入
    @ReplaceWithMock
    private BankDao bankDao;

    @Ordered(0)
    @Test
    public void shouldReturnMockedBalance() {
        given(bankService.getBalanceByEmail("user@bank.com")).willReturn(String.valueOf(valueOf(123.45D)));

        Double balance = userBalanceService.getAccountBalance("user@bank.com");
        String balanceByEmail = bankService.getBalanceByEmail("user@bank.com");
        verify(bankService,times(2)).getBalanceByEmail("user@bank.com");

        assertEquals(balance, valueOf(123.45D));
    }

    @Ordered(1)
    @Test
    public void shouldReturnRealBalance() {

        Double balance = userBalanceService.getAccountBalance("user@bank.com");
        String balanceByEmail = bankService.getBalanceByEmail("user@bank.com");



        verify(bankService,times(2)).getBalanceByEmail("user@bank.com");

        assertEquals(balance,null);
    }
}