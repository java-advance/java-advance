package com.tantal.ad.spring.test;

import com.tantal.ad.spring.dao.BankDao;
import com.tantal.ad.spring.service.BankService;
import com.tantal.ad.spring.service.UserBalanceService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kubek2k.springockito.annotations.ReplaceWithMock;
import org.kubek2k.springockito.annotations.SpringockitoContextLoader;
import org.kubek2k.springockito.annotations.WrapWithSpy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static java.lang.Double.valueOf;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = SpringockitoContextLoader.class, locations = "classpath*:applicationContext.xml")
public class WrapWithSpyTest {

    @Autowired
    private UserBalanceService userBalanceService;

    /*
    @WrapWithSpy 可以监控（Spying）一个Bean，但不影响它的任何行为，注意beanName必须与@Service指定的名称一致

    @WrapWithSpy(beanName = "bankServiceImpl") //否则会抛出异常：

        org.mockito.exceptions.misusing.NotAMockException:
        Argument passed to verify() is not a mock!
        Examples of correct verifications:
        verify(mock).someMethod();
        verify(mock, times(10)).someMethod();
        verify(mock, atLeastOnce()).someMethod();
    */
    @Autowired
    @WrapWithSpy(beanName = "bankServiceImpl")
    private BankService bankService;

    //因为容器确实BankDao 这里需要mock注入
    @ReplaceWithMock
    private BankDao bankDao;

    @Before
    public void setUp() throws Exception {
        when(bankService.getBalanceByEmail("user@bank.com")).thenReturn(String.valueOf(valueOf(123.45D)));
    }
    @Test
    public void shouldReturnMockedBalance() {
        Double balance = userBalanceService.getAccountBalance("user@bank.com");

        String balanceByEmail = bankService.getBalanceByEmail("user@bank.com");
//        verify(bankService, times(2)).getBalanceByEmail("user@bank.com");
//        assertEquals(balance, valueOf(123.45D));
    }
}