package org.kubek2k.tools;

import com.sun.javafx.UnmodifiableArrayList;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

import java.lang.reflect.Method;
import java.util.*;

public class OrderedSpringJUnit4ClassRunner
	extends org.springframework.test.context.junit4.SpringJUnit4ClassRunner
{

	public OrderedSpringJUnit4ClassRunner( final Class< ? > clazz )
		throws InitializationError
	{
		super( clazz );
	}

	@Override
	protected List<FrameworkMethod> computeTestMethods()
	{
		final List<FrameworkMethod> frameworkMethods =this.convertArrayList(super.computeTestMethods());
		if(frameworkMethods instanceof UnmodifiableArrayList)return frameworkMethods;

		Collections.sort( frameworkMethods, new Comparator<FrameworkMethod>()
		{

			public int compare( final FrameworkMethod o1, final FrameworkMethod o2 )
			{
				final Integer i1 = getOrder( o1 );
				final Integer i2 = getOrder( o2 );
				return i1.compareTo( i2 );
			}
		} );
		return frameworkMethods;
	}

	private int getOrder( final FrameworkMethod frameworkMethod )
	{
		final Method method = frameworkMethod.getMethod();
		if ( method.isAnnotationPresent( Ordered.class ) )
		{
			return method.getAnnotation( Ordered.class ).value();
		}
		else
		{
			return Ordered.DEFAULT;
		}
	}

	private List<FrameworkMethod> convertArrayList( List<FrameworkMethod>frameworkMethods){
		final List<FrameworkMethod> retList=new ArrayList<>(frameworkMethods.size());
		retList.addAll(frameworkMethods);
		return retList;
	}
}
